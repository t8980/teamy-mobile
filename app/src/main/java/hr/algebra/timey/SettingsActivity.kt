package hr.algebra.timey

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import hr.algebra.timey.databinding.ActivitySettinggsBinding
import hr.algebra.timey.framework.utils.goToLogin
import hr.algebra.timey.framework.utils.goToMain

class SettingsActivity : AppCompatActivity() {

    lateinit var b: ActivitySettinggsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        b = ActivitySettinggsBinding.inflate(layoutInflater)
        setContentView(b.root)
    }

    override fun onBackPressed() {
        goToMain()
    }
}