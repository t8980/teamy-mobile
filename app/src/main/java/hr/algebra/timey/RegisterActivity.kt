package hr.algebra.timey


import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.AuthFailureError
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import hr.algebra.timey.databinding.ActivityRegisterBinding
import hr.algebra.timey.framework.Constants.DOMAIN
import hr.algebra.timey.framework.utils.goToLogin
import hr.algebra.timey.framework.utils.isEmailValid
import hr.algebra.timey.framework.utils.isValidPassword
import hr.algebra.timey.framework.utils.showToast
import org.json.JSONObject

class RegisterActivity : AppCompatActivity() {

    var volleyRequestQueue: RequestQueue? = null
    val serverAPIURL = DOMAIN + ""
    val TAG = ""

    private lateinit var _binding: ActivityRegisterBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityRegisterBinding.inflate(layoutInflater)
        setContentView(_binding.root)
        setupListeners()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        goToLogin()
    }

    private fun setupListeners() {
        _binding.btnRegister.setOnClickListener {
            register(
                _binding.firstName.toString(),
                _binding.lastName.toString(),
                _binding.uid.text.toString(),
                _binding.pwd.text.toString(),
                _binding.pwd2.text.toString()
            )
        }
    }

    private fun register(
        firstName: String,
        lastName: String,
        uid: String,
        pwd: String,
        pwd2: String
    ) {
        // valid inputs
        if (!validateInput()) {
            return
        }

        // success auth on server
        val registered: Boolean = registerUser(firstName, lastName, uid, pwd, pwd2)
        if (registered) {
            // TODO: goToLogin(uid, pwd) -> fill uid and pwd fields on success register
            //  not high priority!!!!!!!!!
            showToast("success")
            goToLogin()
        } else {
            // fail to register on server
            showToast(getString(R.string.register_failed))
        }
    }

    private fun registerUser(firstName: String, lastName: String, uid: String, pwd: String, pwd2: String): Boolean {
        // TODO: call register api
        return true
    }


    // Checking if the input in form is valid
    private fun validateInput(): Boolean {
        var valid = true

        // first and last name
        if(_binding.firstName.text.isBlank()){
            _binding.firstName.error = getString(R.string.pls_enter_first_name)
            valid = false
        }
        if(_binding.lastName.text.isBlank()){
            _binding.lastName.error = getString(R.string.psl_enter_last_name)
            valid = false
        }

        // checking the proper email format
        if (_binding.uid.text.isBlank() ||
            !isEmailValid(_binding.uid.text.toString())
        ) {
            _binding.uid.error = getString(R.string.pls_enter_valid_email)
            valid = false
        }

        // password validation
        if (!isValidPassword(_binding.pwd.text.toString())
        ) {
            _binding.pwd.error = getString(R.string.password_validation_massage)
            valid = false
        }

        if(_binding.pwd2.text.toString() != _binding.pwd.text.toString()){
            _binding.pwd2.error = getString(R.string.pass_dont_match)
            valid = false
        }
        return valid
    }
}