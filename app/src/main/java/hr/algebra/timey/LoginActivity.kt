package hr.algebra.timey

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.preference.PreferenceManager
import hr.algebra.timey.databinding.ActivityLoginBinding
import hr.algebra.timey.framework.utils.goToMain
import hr.algebra.timey.framework.utils.goToRegister
import hr.algebra.timey.framework.utils.isEmailValid
import hr.algebra.timey.framework.utils.showToast

const val LOGIN_KEY_UID = "hr.algebra.timey.LoginActivity.uidkey"
const val MIN_PASSWORD_LENGTH = 4

class LoginActivity : AppCompatActivity() {

    private lateinit var _binding: ActivityLoginBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(_binding.root)
        setupListeners()
    }

    private fun setupListeners() {
        _binding.btnLogin.setOnClickListener {
            login(_binding.uid.text.toString(), _binding.pwd.text.toString())
        }
        _binding.tvRegister.setOnClickListener {
            goToRegister()
        }
        // TODO: on focus lost validate and show error messages, not high priority!!!!!!!!!
    }

    private fun login(uid: String, pwd: String) {
        // valid inputs
        if (!validateInput()) {
            return
        }
        // success auth on server
        val authenticated: Boolean = authUser(uid, pwd)
        if (authenticated) {
            // save successful login
            PreferenceManager.getDefaultSharedPreferences(this).edit().apply {
                this.putString(LOGIN_KEY_UID, uid)
                apply()
            }
            goToMain()
        } else {
            // fail to auth on server


            showToast(getString(R.string.wrong_uid_or_pwd))
        }
    }

    // validate input beforehand
    private fun authUser(uid: String, pwd: String): Boolean {
        // TODO: call auth api
        return true
    }

    // Checking if the input in form is valid
    private fun validateInput(): Boolean {
        var valid = true

        // checking the proper email format
        if (_binding.uid.text.isBlank() ||
            !isEmailValid(_binding.uid.text.toString())
        ) {
            _binding.uid.error = getString(R.string.pls_enter_valid_email)
            valid = false
        }

        // password validation
        if (_binding.pwd.text.isBlank() ||
            _binding.pwd.text.length < MIN_PASSWORD_LENGTH
        ) {
            _binding.pwd.error = getString(R.string.pls_enter_valid_pwd)
            valid = false
        }
        return valid
    }


}