package hr.algebra.timey.framework.utils

import android.app.Activity
import android.content.Intent
import hr.algebra.timey.LoginActivity
import hr.algebra.timey.MainActivity
import hr.algebra.timey.RegisterActivity
import hr.algebra.timey.SettingsActivity

fun Activity.goToLogin() {
    startActivity<LoginActivity>()
}

fun Activity.goToRegister() {
    startActivity<RegisterActivity>()
}

fun Activity.goToMain() {
    startActivity<MainActivity>()
}

fun Activity.showSettings() {
    startActivity<SettingsActivity>()
}
