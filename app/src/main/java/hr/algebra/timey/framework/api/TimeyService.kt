package hr.algebra.timey.framework.api

import hr.algebra.timey.framework.Constants
import hr.algebra.timey.framework.models.User
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface TimeyService {

    //returns all users
    @GET(Constants.DOMAIN + "User")
    fun getAllUsers(): Response<User>

    //get method that returns user with the id if user does not exist it returns null
    @GET(Constants.DOMAIN + "User/id={user-id}")
    fun getUsers(
        @Query("user-id") q: String,
    ): Response<User>

    //Get method that returns id of user if pwd and uname are correct and returns -1 if not
    @GET(Constants.DOMAIN + "User/uname={username}&pwd={pwd}")
    fun getLoginUser(
        @Query("username") username:String,
        @Query("pwd") pwd:String
    )

    @GET(Constants.DOMAIN+"Teams")
    fun getTeam(
    )

}
