package hr.algebra.timey.framework.models

data class User(val uid: String, val pwd: String)